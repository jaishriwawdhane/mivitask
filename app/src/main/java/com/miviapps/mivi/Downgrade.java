
package com.miviapps.mivi;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Downgrade implements Parcelable
{

    @SerializedName("data")
    @Expose
    private Object data;
    public final static Creator<Downgrade> CREATOR = new Creator<Downgrade>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Downgrade createFromParcel(Parcel in) {
            return new Downgrade(in);
        }

        public Downgrade[] newArray(int size) {
            return (new Downgrade[size]);
        }

    }
    ;

    protected Downgrade(Parcel in) {
        this.data = ((Object) in.readValue((Object.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Downgrade() {
    }

    /**
     * 
     * @param data
     */
    public Downgrade(Object data) {
        super();
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(data);
    }

    public int describeContents() {
        return  0;
    }

}
