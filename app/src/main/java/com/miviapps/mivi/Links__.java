
package com.miviapps.mivi;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Links__ implements Parcelable
{

    @SerializedName("self")
    @Expose
    private String self;
    public final static Creator<Links__> CREATOR = new Creator<Links__>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Links__ createFromParcel(Parcel in) {
            return new Links__(in);
        }

        public Links__[] newArray(int size) {
            return (new Links__[size]);
        }

    }
    ;

    protected Links__(Parcel in) {
        this.self = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Links__() {
    }

    /**
     * 
     * @param self
     */
    public Links__(String self) {
        super();
        this.self = self;
    }

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(self);
    }

    public int describeContents() {
        return  0;
    }

}
