
package com.miviapps.mivi;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MiviObjet implements Parcelable
{

    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("included")
    @Expose
    private List<Included> included = null;
    public final static Creator<MiviObjet> CREATOR = new Creator<MiviObjet>() {


        @SuppressWarnings({
            "unchecked"
        })
        public MiviObjet createFromParcel(Parcel in) {
            return new MiviObjet(in);
        }

        public MiviObjet[] newArray(int size) {
            return (new MiviObjet[size]);
        }

    }
    ;

    protected MiviObjet(Parcel in) {
        this.data = ((Data) in.readValue((Data.class.getClassLoader())));
        in.readList(this.included, (Included.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public MiviObjet() {
    }

    /**
     * 
     * @param data
     * @param included
     */
    public MiviObjet(Data data, List<Included> included) {
        super();
        this.data = data;
        this.included = included;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public List<Included> getIncluded() {
        return included;
    }

    public void setIncluded(List<Included> included) {
        this.included = included;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(data);
        dest.writeList(included);
    }

    public int describeContents() {
        return  0;
    }

}
