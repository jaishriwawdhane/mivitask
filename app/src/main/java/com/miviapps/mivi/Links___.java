
package com.miviapps.mivi;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Links___ implements Parcelable
{

    @SerializedName("related")
    @Expose
    private String related;
    public final static Creator<Links___> CREATOR = new Creator<Links___>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Links___ createFromParcel(Parcel in) {
            return new Links___(in);
        }

        public Links___[] newArray(int size) {
            return (new Links___[size]);
        }

    }
    ;

    protected Links___(Parcel in) {
        this.related = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Links___() {
    }

    /**
     * 
     * @param related
     */
    public Links___(String related) {
        super();
        this.related = related;
    }

    public String getRelated() {
        return related;
    }

    public void setRelated(String related) {
        this.related = related;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(related);
    }

    public int describeContents() {
        return  0;
    }

}
