
package com.miviapps.mivi;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Subscriptions implements Parcelable
{

    @SerializedName("links")
    @Expose
    private Links___ links;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    public final static Creator<Subscriptions> CREATOR = new Creator<Subscriptions>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Subscriptions createFromParcel(Parcel in) {
            return new Subscriptions(in);
        }

        public Subscriptions[] newArray(int size) {
            return (new Subscriptions[size]);
        }

    }
    ;

    protected Subscriptions(Parcel in) {
        this.links = ((Links___) in.readValue((Links___.class.getClassLoader())));
        in.readList(this.data, (Datum.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Subscriptions() {
    }

    /**
     * 
     * @param data
     * @param links
     */
    public Subscriptions(Links___ links, List<Datum> data) {
        super();
        this.links = links;
        this.data = data;
    }

    public Links___ getLinks() {
        return links;
    }

    public void setLinks(Links___ links) {
        this.links = links;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(links);
        dest.writeList(data);
    }

    public int describeContents() {
        return  0;
    }

}
