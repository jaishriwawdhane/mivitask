
package com.miviapps.mivi;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Links____ implements Parcelable
{

    @SerializedName("related")
    @Expose
    private String related;
    public final static Creator<Links____> CREATOR = new Creator<Links____>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Links____ createFromParcel(Parcel in) {
            return new Links____(in);
        }

        public Links____[] newArray(int size) {
            return (new Links____[size]);
        }

    }
    ;

    protected Links____(Parcel in) {
        this.related = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Links____() {
    }

    /**
     * 
     * @param related
     */
    public Links____(String related) {
        super();
        this.related = related;
    }

    public String getRelated() {
        return related;
    }

    public void setRelated(String related) {
        this.related = related;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(related);
    }

    public int describeContents() {
        return  0;
    }

}
