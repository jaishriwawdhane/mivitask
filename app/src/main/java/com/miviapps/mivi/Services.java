
package com.miviapps.mivi;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Services implements Parcelable
{

    @SerializedName("links")
    @Expose
    private Links_ links;
    public final static Creator<Services> CREATOR = new Creator<Services>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Services createFromParcel(Parcel in) {
            return new Services(in);
        }

        public Services[] newArray(int size) {
            return (new Services[size]);
        }

    }
    ;

    protected Services(Parcel in) {
        this.links = ((Links_) in.readValue((Links_.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Services() {
    }

    /**
     * 
     * @param links
     */
    public Services(Links_ links) {
        super();
        this.links = links;
    }

    public Links_ getLinks() {
        return links;
    }

    public void setLinks(Links_ links) {
        this.links = links;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(links);
    }

    public int describeContents() {
        return  0;
    }

}
