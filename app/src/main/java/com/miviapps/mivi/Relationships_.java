
package com.miviapps.mivi;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Relationships_ implements Parcelable
{

    @SerializedName("subscriptions")
    @Expose
    private Subscriptions subscriptions;
    @SerializedName("service")
    @Expose
    private Service service;
    @SerializedName("product")
    @Expose
    private Product product;
    @SerializedName("downgrade")
    @Expose
    private Downgrade downgrade;
    public final static Creator<Relationships_> CREATOR = new Creator<Relationships_>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Relationships_ createFromParcel(Parcel in) {
            return new Relationships_(in);
        }

        public Relationships_[] newArray(int size) {
            return (new Relationships_[size]);
        }

    }
    ;

    protected Relationships_(Parcel in) {
        this.subscriptions = ((Subscriptions) in.readValue((Subscriptions.class.getClassLoader())));
        this.service = ((Service) in.readValue((Service.class.getClassLoader())));
        this.product = ((Product) in.readValue((Product.class.getClassLoader())));
        this.downgrade = ((Downgrade) in.readValue((Downgrade.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Relationships_() {
    }

    /**
     * 
     * @param product
     * @param subscriptions
     * @param service
     * @param downgrade
     */
    public Relationships_(Subscriptions subscriptions, Service service, Product product, Downgrade downgrade) {
        super();
        this.subscriptions = subscriptions;
        this.service = service;
        this.product = product;
        this.downgrade = downgrade;
    }

    public Subscriptions getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(Subscriptions subscriptions) {
        this.subscriptions = subscriptions;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Downgrade getDowngrade() {
        return downgrade;
    }

    public void setDowngrade(Downgrade downgrade) {
        this.downgrade = downgrade;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(subscriptions);
        dest.writeValue(service);
        dest.writeValue(product);
        dest.writeValue(downgrade);
    }

    public int describeContents() {
        return  0;
    }

}
