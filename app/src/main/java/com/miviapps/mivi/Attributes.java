
package com.miviapps.mivi;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attributes implements Parcelable
{

    @SerializedName("payment-type")
    @Expose
    private String paymentType;
    @SerializedName("unbilled-charges")
    @Expose
    private Object unbilledCharges;
    @SerializedName("next-billing-date")
    @Expose
    private Object nextBillingDate;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("first-name")
    @Expose
    private String firstName;
    @SerializedName("last-name")
    @Expose
    private String lastName;
    @SerializedName("date-of-birth")
    @Expose
    private String dateOfBirth;
    @SerializedName("contact-number")
    @Expose
    private String contactNumber;
    @SerializedName("email-address")
    @Expose
    private String emailAddress;
    @SerializedName("email-address-verified")
    @Expose
    private boolean emailAddressVerified;
    @SerializedName("email-subscription-status")
    @Expose
    private boolean emailSubscriptionStatus;
    public final static Creator<Attributes> CREATOR = new Creator<Attributes>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Attributes createFromParcel(Parcel in) {
            return new Attributes(in);
        }

        public Attributes[] newArray(int size) {
            return (new Attributes[size]);
        }

    }
    ;

    protected Attributes(Parcel in) {
        this.paymentType = ((String) in.readValue((String.class.getClassLoader())));
        this.unbilledCharges = ((Object) in.readValue((Object.class.getClassLoader())));
        this.nextBillingDate = ((Object) in.readValue((Object.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.firstName = ((String) in.readValue((String.class.getClassLoader())));
        this.lastName = ((String) in.readValue((String.class.getClassLoader())));
        this.dateOfBirth = ((String) in.readValue((String.class.getClassLoader())));
        this.contactNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.emailAddress = ((String) in.readValue((String.class.getClassLoader())));
        this.emailAddressVerified = ((boolean) in.readValue((boolean.class.getClassLoader())));
        this.emailSubscriptionStatus = ((boolean) in.readValue((boolean.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Attributes() {
    }

    /**
     * 
     * @param dateOfBirth
     * @param lastName
     * @param contactNumber
     * @param title
     * @param emailSubscriptionStatus
     * @param paymentType
     * @param nextBillingDate
     * @param emailAddressVerified
     * @param emailAddress
     * @param firstName
     * @param unbilledCharges
     */
    public Attributes(String paymentType, Object unbilledCharges, Object nextBillingDate, String title, String firstName, String lastName, String dateOfBirth, String contactNumber, String emailAddress, boolean emailAddressVerified, boolean emailSubscriptionStatus) {
        super();
        this.paymentType = paymentType;
        this.unbilledCharges = unbilledCharges;
        this.nextBillingDate = nextBillingDate;
        this.title = title;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.contactNumber = contactNumber;
        this.emailAddress = emailAddress;
        this.emailAddressVerified = emailAddressVerified;
        this.emailSubscriptionStatus = emailSubscriptionStatus;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public Object getUnbilledCharges() {
        return unbilledCharges;
    }

    public void setUnbilledCharges(Object unbilledCharges) {
        this.unbilledCharges = unbilledCharges;
    }

    public Object getNextBillingDate() {
        return nextBillingDate;
    }

    public void setNextBillingDate(Object nextBillingDate) {
        this.nextBillingDate = nextBillingDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public boolean isEmailAddressVerified() {
        return emailAddressVerified;
    }

    public void setEmailAddressVerified(boolean emailAddressVerified) {
        this.emailAddressVerified = emailAddressVerified;
    }

    public boolean isEmailSubscriptionStatus() {
        return emailSubscriptionStatus;
    }

    public void setEmailSubscriptionStatus(boolean emailSubscriptionStatus) {
        this.emailSubscriptionStatus = emailSubscriptionStatus;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(paymentType);
        dest.writeValue(unbilledCharges);
        dest.writeValue(nextBillingDate);
        dest.writeValue(title);
        dest.writeValue(firstName);
        dest.writeValue(lastName);
        dest.writeValue(dateOfBirth);
        dest.writeValue(contactNumber);
        dest.writeValue(emailAddress);
        dest.writeValue(emailAddressVerified);
        dest.writeValue(emailSubscriptionStatus);
    }

    public int describeContents() {
        return  0;
    }

}
