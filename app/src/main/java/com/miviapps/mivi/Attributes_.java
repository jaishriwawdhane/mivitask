
package com.miviapps.mivi;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attributes_ implements Parcelable
{

    @SerializedName("msn")
    @Expose
    private String msn;
    @SerializedName("credit")
    @Expose
    private int credit;
    @SerializedName("credit-expiry")
    @Expose
    private String creditExpiry;
    @SerializedName("data-usage-threshold")
    @Expose
    private boolean dataUsageThreshold;
    @SerializedName("included-data-balance")
    @Expose
    private int includedDataBalance;
    @SerializedName("included-credit-balance")
    @Expose
    private Object includedCreditBalance;
    @SerializedName("included-rollover-credit-balance")
    @Expose
    private Object includedRolloverCreditBalance;
    @SerializedName("included-rollover-data-balance")
    @Expose
    private Object includedRolloverDataBalance;
    @SerializedName("included-international-talk-balance")
    @Expose
    private Object includedInternationalTalkBalance;
    @SerializedName("expiry-date")
    @Expose
    private String expiryDate;
    @SerializedName("auto-renewal")
    @Expose
    private boolean autoRenewal;
    @SerializedName("primary-subscription")
    @Expose
    private boolean primarySubscription;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("included-data")
    @Expose
    private Object includedData;
    @SerializedName("included-credit")
    @Expose
    private Object includedCredit;
    @SerializedName("included-international-talk")
    @Expose
    private Object includedInternationalTalk;
    @SerializedName("unlimited-text")
    @Expose
    private boolean unlimitedText;
    @SerializedName("unlimited-talk")
    @Expose
    private boolean unlimitedTalk;
    @SerializedName("unlimited-international-text")
    @Expose
    private boolean unlimitedInternationalText;
    @SerializedName("unlimited-international-talk")
    @Expose
    private boolean unlimitedInternationalTalk;
    @SerializedName("price")
    @Expose
    private int price;
    public final static Creator<Attributes_> CREATOR = new Creator<Attributes_>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Attributes_ createFromParcel(Parcel in) {
            return new Attributes_(in);
        }

        public Attributes_[] newArray(int size) {
            return (new Attributes_[size]);
        }

    }
    ;

    protected Attributes_(Parcel in) {
        this.msn = ((String) in.readValue((String.class.getClassLoader())));
        this.credit = ((int) in.readValue((int.class.getClassLoader())));
        this.creditExpiry = ((String) in.readValue((String.class.getClassLoader())));
        this.dataUsageThreshold = ((boolean) in.readValue((boolean.class.getClassLoader())));
        this.includedDataBalance = ((int) in.readValue((int.class.getClassLoader())));
        this.includedCreditBalance = ((Object) in.readValue((Object.class.getClassLoader())));
        this.includedRolloverCreditBalance = ((Object) in.readValue((Object.class.getClassLoader())));
        this.includedRolloverDataBalance = ((Object) in.readValue((Object.class.getClassLoader())));
        this.includedInternationalTalkBalance = ((Object) in.readValue((Object.class.getClassLoader())));
        this.expiryDate = ((String) in.readValue((String.class.getClassLoader())));
        this.autoRenewal = ((boolean) in.readValue((boolean.class.getClassLoader())));
        this.primarySubscription = ((boolean) in.readValue((boolean.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.includedData = ((Object) in.readValue((Object.class.getClassLoader())));
        this.includedCredit = ((Object) in.readValue((Object.class.getClassLoader())));
        this.includedInternationalTalk = ((Object) in.readValue((Object.class.getClassLoader())));
        this.unlimitedText = ((boolean) in.readValue((boolean.class.getClassLoader())));
        this.unlimitedTalk = ((boolean) in.readValue((boolean.class.getClassLoader())));
        this.unlimitedInternationalText = ((boolean) in.readValue((boolean.class.getClassLoader())));
        this.unlimitedInternationalTalk = ((boolean) in.readValue((boolean.class.getClassLoader())));
        this.price = ((int) in.readValue((int.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Attributes_() {
    }

    /**
     * 
     * @param includedCredit
     * @param includedData
     * @param msn
     * @param includedDataBalance
     * @param unlimitedInternationalText
     * @param expiryDate
     * @param primarySubscription
     * @param dataUsageThreshold
     * @param autoRenewal
     * @param unlimitedInternationalTalk
     * @param includedCreditBalance
     * @param creditExpiry
     * @param unlimitedTalk
     * @param price
     * @param name
     * @param includedInternationalTalk
     * @param unlimitedText
     * @param includedRolloverDataBalance
     * @param credit
     * @param includedRolloverCreditBalance
     * @param includedInternationalTalkBalance
     */
    public Attributes_(String msn, int credit, String creditExpiry, boolean dataUsageThreshold, int includedDataBalance, Object includedCreditBalance, Object includedRolloverCreditBalance, Object includedRolloverDataBalance, Object includedInternationalTalkBalance, String expiryDate, boolean autoRenewal, boolean primarySubscription, String name, Object includedData, Object includedCredit, Object includedInternationalTalk, boolean unlimitedText, boolean unlimitedTalk, boolean unlimitedInternationalText, boolean unlimitedInternationalTalk, int price) {
        super();
        this.msn = msn;
        this.credit = credit;
        this.creditExpiry = creditExpiry;
        this.dataUsageThreshold = dataUsageThreshold;
        this.includedDataBalance = includedDataBalance;
        this.includedCreditBalance = includedCreditBalance;
        this.includedRolloverCreditBalance = includedRolloverCreditBalance;
        this.includedRolloverDataBalance = includedRolloverDataBalance;
        this.includedInternationalTalkBalance = includedInternationalTalkBalance;
        this.expiryDate = expiryDate;
        this.autoRenewal = autoRenewal;
        this.primarySubscription = primarySubscription;
        this.name = name;
        this.includedData = includedData;
        this.includedCredit = includedCredit;
        this.includedInternationalTalk = includedInternationalTalk;
        this.unlimitedText = unlimitedText;
        this.unlimitedTalk = unlimitedTalk;
        this.unlimitedInternationalText = unlimitedInternationalText;
        this.unlimitedInternationalTalk = unlimitedInternationalTalk;
        this.price = price;
    }

    public String getMsn() {
        return msn;
    }

    public void setMsn(String msn) {
        this.msn = msn;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public String getCreditExpiry() {
        return creditExpiry;
    }

    public void setCreditExpiry(String creditExpiry) {
        this.creditExpiry = creditExpiry;
    }

    public boolean isDataUsageThreshold() {
        return dataUsageThreshold;
    }

    public void setDataUsageThreshold(boolean dataUsageThreshold) {
        this.dataUsageThreshold = dataUsageThreshold;
    }

    public int getIncludedDataBalance() {
        return includedDataBalance;
    }

    public void setIncludedDataBalance(int includedDataBalance) {
        this.includedDataBalance = includedDataBalance;
    }

    public Object getIncludedCreditBalance() {
        return includedCreditBalance;
    }

    public void setIncludedCreditBalance(Object includedCreditBalance) {
        this.includedCreditBalance = includedCreditBalance;
    }

    public Object getIncludedRolloverCreditBalance() {
        return includedRolloverCreditBalance;
    }

    public void setIncludedRolloverCreditBalance(Object includedRolloverCreditBalance) {
        this.includedRolloverCreditBalance = includedRolloverCreditBalance;
    }

    public Object getIncludedRolloverDataBalance() {
        return includedRolloverDataBalance;
    }

    public void setIncludedRolloverDataBalance(Object includedRolloverDataBalance) {
        this.includedRolloverDataBalance = includedRolloverDataBalance;
    }

    public Object getIncludedInternationalTalkBalance() {
        return includedInternationalTalkBalance;
    }

    public void setIncludedInternationalTalkBalance(Object includedInternationalTalkBalance) {
        this.includedInternationalTalkBalance = includedInternationalTalkBalance;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public boolean isAutoRenewal() {
        return autoRenewal;
    }

    public void setAutoRenewal(boolean autoRenewal) {
        this.autoRenewal = autoRenewal;
    }

    public boolean isPrimarySubscription() {
        return primarySubscription;
    }

    public void setPrimarySubscription(boolean primarySubscription) {
        this.primarySubscription = primarySubscription;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getIncludedData() {
        return includedData;
    }

    public void setIncludedData(Object includedData) {
        this.includedData = includedData;
    }

    public Object getIncludedCredit() {
        return includedCredit;
    }

    public void setIncludedCredit(Object includedCredit) {
        this.includedCredit = includedCredit;
    }

    public Object getIncludedInternationalTalk() {
        return includedInternationalTalk;
    }

    public void setIncludedInternationalTalk(Object includedInternationalTalk) {
        this.includedInternationalTalk = includedInternationalTalk;
    }

    public boolean isUnlimitedText() {
        return unlimitedText;
    }

    public void setUnlimitedText(boolean unlimitedText) {
        this.unlimitedText = unlimitedText;
    }

    public boolean isUnlimitedTalk() {
        return unlimitedTalk;
    }

    public void setUnlimitedTalk(boolean unlimitedTalk) {
        this.unlimitedTalk = unlimitedTalk;
    }

    public boolean isUnlimitedInternationalText() {
        return unlimitedInternationalText;
    }

    public void setUnlimitedInternationalText(boolean unlimitedInternationalText) {
        this.unlimitedInternationalText = unlimitedInternationalText;
    }

    public boolean isUnlimitedInternationalTalk() {
        return unlimitedInternationalTalk;
    }

    public void setUnlimitedInternationalTalk(boolean unlimitedInternationalTalk) {
        this.unlimitedInternationalTalk = unlimitedInternationalTalk;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(msn);
        dest.writeValue(credit);
        dest.writeValue(creditExpiry);
        dest.writeValue(dataUsageThreshold);
        dest.writeValue(includedDataBalance);
        dest.writeValue(includedCreditBalance);
        dest.writeValue(includedRolloverCreditBalance);
        dest.writeValue(includedRolloverDataBalance);
        dest.writeValue(includedInternationalTalkBalance);
        dest.writeValue(expiryDate);
        dest.writeValue(autoRenewal);
        dest.writeValue(primarySubscription);
        dest.writeValue(name);
        dest.writeValue(includedData);
        dest.writeValue(includedCredit);
        dest.writeValue(includedInternationalTalk);
        dest.writeValue(unlimitedText);
        dest.writeValue(unlimitedTalk);
        dest.writeValue(unlimitedInternationalText);
        dest.writeValue(unlimitedInternationalTalk);
        dest.writeValue(price);
    }

    public int describeContents() {
        return  0;
    }

}
