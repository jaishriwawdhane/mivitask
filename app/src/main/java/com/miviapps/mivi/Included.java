
package com.miviapps.mivi;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Included implements Parcelable
{

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("attributes")
    @Expose
    private Attributes_ attributes;
    @SerializedName("links")
    @Expose
    private Links__ links;
    @SerializedName("relationships")
    @Expose
    private Relationships_ relationships;
    public final static Creator<Included> CREATOR = new Creator<Included>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Included createFromParcel(Parcel in) {
            return new Included(in);
        }

        public Included[] newArray(int size) {
            return (new Included[size]);
        }

    }
    ;

    protected Included(Parcel in) {
        this.type = ((String) in.readValue((String.class.getClassLoader())));
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.attributes = ((Attributes_) in.readValue((Attributes_.class.getClassLoader())));
        this.links = ((Links__) in.readValue((Links__.class.getClassLoader())));
        this.relationships = ((Relationships_) in.readValue((Relationships_.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Included() {
    }

    /**
     * 
     * @param id
     * @param links
     * @param attributes
     * @param type
     * @param relationships
     */
    public Included(String type, String id, Attributes_ attributes, Links__ links, Relationships_ relationships) {
        super();
        this.type = type;
        this.id = id;
        this.attributes = attributes;
        this.links = links;
        this.relationships = relationships;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Attributes_ getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes_ attributes) {
        this.attributes = attributes;
    }

    public Links__ getLinks() {
        return links;
    }

    public void setLinks(Links__ links) {
        this.links = links;
    }

    public Relationships_ getRelationships() {
        return relationships;
    }

    public void setRelationships(Relationships_ relationships) {
        this.relationships = relationships;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(type);
        dest.writeValue(id);
        dest.writeValue(attributes);
        dest.writeValue(links);
        dest.writeValue(relationships);
    }

    public int describeContents() {
        return  0;
    }

}
