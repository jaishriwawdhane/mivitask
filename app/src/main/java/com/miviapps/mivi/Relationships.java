
package com.miviapps.mivi;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Relationships implements Parcelable
{

    @SerializedName("services")
    @Expose
    private Services services;
    public final static Creator<Relationships> CREATOR = new Creator<Relationships>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Relationships createFromParcel(Parcel in) {
            return new Relationships(in);
        }

        public Relationships[] newArray(int size) {
            return (new Relationships[size]);
        }

    }
    ;

    protected Relationships(Parcel in) {
        this.services = ((Services) in.readValue((Services.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Relationships() {
    }

    /**
     * 
     * @param services
     */
    public Relationships(Services services) {
        super();
        this.services = services;
    }

    public Services getServices() {
        return services;
    }

    public void setServices(Services services) {
        this.services = services;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(services);
    }

    public int describeContents() {
        return  0;
    }

}
