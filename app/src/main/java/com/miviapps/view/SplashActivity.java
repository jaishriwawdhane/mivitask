package com.miviapps.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.miviapps.R;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Observer;
import rx.Subscription;

/**
 * Created by jaishri on 14/10/18.
 */

public class SplashActivity extends AppCompatActivity{
    private Subscription subscription;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }




    @Override
    public void onBackPressed() {
        subscription.unsubscribe();
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        subscription.unsubscribe();
        super.onPause();
    }

    @Override
    protected void onResume() {
        subscription = Observable.timer(2, TimeUnit.SECONDS).subscribe(observer);
        super.onResume();
    }

    Observer observer = new Observer() {
        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {

            e.printStackTrace();
        }

        @Override
        public void onNext(Object o) {
            Intent intent = new Intent(SplashActivity.this, UserDetails.class);
            startActivity(intent);
            finish();
        }
    };
}
