package com.miviapps.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.miviapps.R;
import com.miviapps.mivi.Attributes;
import com.miviapps.mivi.Data;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func0;
import rx.schedulers.Schedulers;

/**
 * Created by jaishri on 14/10/18.
 */

public class MiviActivity extends AppCompatActivity {

    private static final String TAG = "MiviActivity";
    private String emailId = "";

    @BindView(R.id.login)
    EditText login;
    @BindView(R.id.validate)
    Button validate;

    private Unbinder unbinder;
    private boolean validLogin = false;

    private Subscription subscription;

    @OnClick(R.id.validate)
    void validateLogin(){
        if (login.getText().toString().equals(emailId))
            startActivity(new Intent(this, com.miviapps.view.SplashActivity.class));
        else
            Toast.makeText(this, "Please verify your email firstand try again!", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        unbinder = ButterKnife.bind(this);

            subscription = getObservedData()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<String>() {
                        @Override
                        public void onCompleted() {
                            Log.d(TAG, "onCompleted: ");
                            makeToast("onCompleted");
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.d(TAG, "onError: " + e.getMessage());
                            makeToast("error");
                        }

                        @Override
                        public void onNext(String mailId) {
                            Log.d(TAG, "onNext: ");
                            emailId = mailId;


                        }
                    });


            Log.d(TAG, String.valueOf(validLogin));



    }

    public String loadJSONFromAsset_login(InputStream is) {
        String json = "";
        try {

            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

            if (json == null || json.equals("")) {

                Toast.makeText(this, "Reponse is null", Toast.LENGTH_SHORT).show();
                return "";
            } else {

                Data data = new Data();
                Attributes attributes = new Attributes();

                JSONObject obj = new JSONObject(json);
                if (obj.has("data")) {
                    JSONObject dataObject = obj.optJSONObject("data");

                    if (dataObject.has("type"))
                        data.setType(dataObject.optString("type"));

                    if (dataObject.has("id"))
                        data.setId(dataObject.optString("id"));
                    if (dataObject.has("attributes")) {
                        JSONObject attributesObject = dataObject.optJSONObject("attributes");

                        if (attributesObject.has("email-address")){
                            attributes.setEmailAddress(attributesObject.optString("email-address"));
                            emailId = attributesObject.optString("email-address");
                        }
                        if (attributesObject.has("email-address-verified")) {
                            attributes.setEmailAddressVerified(attributesObject.optBoolean("email-address-verified"));


                        }
                        if (attributesObject.has("email-subscription-status"))
                            attributes.setEmailSubscriptionStatus(attributesObject.optBoolean("email-subscription-status"));

                        data.setAttributes(attributes);

                    }

                }
                return emailId;
            }

            } catch(IOException ex){
                ex.printStackTrace();
                return null;
            } catch(JSONException e){
                e.printStackTrace();
            }
            return null;

    }


    public Observable<String> getObservedData() {
        return Observable.defer(new Func0<Observable<String>>() {
            @Override
            public Observable<String> call() {
                try {
                    return Observable.just(loadJSONFromAsset_login(getAssets().open("collections.json")));
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        });

    }

    private void makeToast(String message) {
        //Toast.makeText(this, "We are in : " + message, Toast.LENGTH_SHORT).show();
    }

}
