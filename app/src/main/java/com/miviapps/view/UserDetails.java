package com.miviapps.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.miviapps.R;
import com.miviapps.adapters.IncludesAdapter;
import com.miviapps.mivi.Attributes;
import com.miviapps.mivi.Attributes_;
import com.miviapps.mivi.Data;
import com.miviapps.mivi.Data_;
import com.miviapps.mivi.Datum;
import com.miviapps.mivi.Downgrade;
import com.miviapps.mivi.Included;
import com.miviapps.mivi.Links;
import com.miviapps.mivi.Links_;
import com.miviapps.mivi.Links__;
import com.miviapps.mivi.Links___;
import com.miviapps.mivi.Links____;
import com.miviapps.mivi.MiviObjet;
import com.miviapps.mivi.Product;
import com.miviapps.mivi.Relationships;
import com.miviapps.mivi.Relationships_;
import com.miviapps.mivi.Service;
import com.miviapps.mivi.Services;
import com.miviapps.mivi.Subscriptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func0;
import rx.schedulers.Schedulers;

/**
 * Created by jaishri on 14/10/18.
 */

public class UserDetails extends Activity {

    private static final String TAG = "UserDetails";
    @BindView(R.id.userName)
    TextView userName;
    @BindView(R.id.birtDate)
    TextView userBirthDate;
    @BindView(R.id.contactNumber)
    TextView userContact;
    @BindView(R.id.emailAddress)
    TextView userEmailAddress;
    @BindView(R.id.isEmailVerified)
    TextView userIsEmailVerified;
    @BindView(R.id.isSubscribed)
    TextView userIsSubscribed;
    @BindView(R.id.type)
    TextView userType;
    @BindView(R.id.idValue)
    TextView userId;
    @BindView(R.id.linkValue)
    TextView linkValue;
    @BindView(R.id.relationType)
    TextView userRelationType;
    @BindView(R.id.relationLink)
    TextView userRelationLink;
    @BindView(R.id.includeRecyclerView)
    RecyclerView includeRecyclerView;
    private Unbinder unbinder;
    private Subscription subscription;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        unbinder = ButterKnife.bind(this);

        subscription = getObservedData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<MiviObjet>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted: ");
                        makeToast("onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: " + e.getMessage());
                        makeToast("eror");
                    }

                    @Override
                    public void onNext(MiviObjet miviObjet) {
                        Log.d(TAG, "onNext: ");

                        setData(miviObjet);

                    }
                });


    }

    private void setData(MiviObjet miviObjet) {

        userName.setText(miviObjet.getData().getAttributes().getTitle() + " " +miviObjet.getData().getAttributes().getFirstName() + " " +
                miviObjet.getData().getAttributes().getLastName());

        userBirthDate.setText(miviObjet.getData().getAttributes().getDateOfBirth());
        userContact.setText(miviObjet.getData().getAttributes().getContactNumber());
        userEmailAddress.setText(miviObjet.getData().getAttributes().getEmailAddress());
        userIsEmailVerified.setText(String.valueOf(miviObjet.getData().getAttributes().isEmailAddressVerified()));
        userIsSubscribed.setText(String.valueOf(miviObjet.getData().getAttributes().isEmailSubscriptionStatus()));
        userType.setText(miviObjet.getData().getType());
        userId.setText(miviObjet.getData().getId());
        linkValue.setText(miviObjet.getData().getLinks().getSelf());
        if (miviObjet.getData().getRelationships().getServices() != null)
            userRelationType.setText("Services");

        userRelationLink.setText(miviObjet.getData().getRelationships().getServices().getLinks().getRelated());

        //

        if (miviObjet.getIncluded() != null) {
            List<Included> mListIncludeds = miviObjet.getIncluded();
            Log.d(TAG, mListIncludeds.size()+"");

            for (int i = 0; i < mListIncludeds.size(); i++) {

                Log.d(TAG, mListIncludeds.get(i).getType());

            }
            LinearLayoutManager llm = new LinearLayoutManager(this);
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            includeRecyclerView.setLayoutManager(llm);
            includeRecyclerView.setAdapter(new IncludesAdapter(this, mListIncludeds));
        }
    }

    public MiviObjet loadJSONFromAsset(InputStream is) {
        MiviObjet miviObjet = new MiviObjet();
        String json = "";
        try {

            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

            if (json == null || json.equals("")) {

                Toast.makeText(this, "Reponse is null", Toast.LENGTH_SHORT).show();
                return null;
            } else {


                JSONObject obj = new JSONObject(json);
                if (obj.has("data")) {
                    JSONObject dataObject = obj.optJSONObject("data");
                    Data data = new Data();
                    if (dataObject.has("type"))
                        data.setType(dataObject.optString("type"));

                    if (dataObject.has("id"))
                        data.setId(dataObject.optString("id"));

                    if (dataObject.has("attributes")) {
                        JSONObject attributesObject = dataObject.optJSONObject("attributes");

                        Attributes attributes = new Attributes();
                        if (attributesObject.has("payment-type"))
                            attributes.setPaymentType(attributesObject.optString("payment-type"));
                        if (attributesObject.has("unbilled-charges"))
                            attributes.setUnbilledCharges(attributesObject.optString("unbilled-charges"));
                        if (attributesObject.has("next-billing-date"))
                            attributes.setNextBillingDate(attributesObject.optString("next-billing-date"));
                        if (attributesObject.has("title"))
                            attributes.setTitle(attributesObject.optString("title"));
                        if (attributesObject.has("first-name"))
                            attributes.setFirstName(attributesObject.optString("first-name"));
                        if (attributesObject.has("last-name"))
                            attributes.setLastName(attributesObject.optString("last-name"));

                        if (attributesObject.has("date-of-birth"))
                            attributes.setDateOfBirth(attributesObject.optString("date-of-birth"));
                        if (attributesObject.has("contact-number"))
                            attributes.setContactNumber(attributesObject.optString("contact-number"));
                        if (attributesObject.has("email-address")) {
                            attributes.setEmailAddress(attributesObject.optString("email-address"));
                        }
                        if (attributesObject.has("email-address-verified")) {
                            attributes.setEmailAddressVerified(attributesObject.optBoolean("email-address-verified"));

                        }
                        if (attributesObject.has("email-subscription-status"))
                            attributes.setEmailSubscriptionStatus(attributesObject.optBoolean("email-subscription-status"));

                        data.setAttributes(attributes);

                    }

                    if (dataObject.has("links")) {
                        Links links = new Links();


                        JSONObject linksJsonObject = dataObject.getJSONObject("links");
                        if (linksJsonObject.has("self")) {
                            links.setSelf(linksJsonObject.optString("self"));
                        }
                        data.setLinks(links);
                    }

                    if (dataObject.has("relationships")) {
                        Relationships relationships = new Relationships();
                        JSONObject relationshipsObject = dataObject.optJSONObject("relationships");
                        if (relationshipsObject.has("services")) {
                            Services services = new Services();

                            JSONObject serviesObject = relationshipsObject.getJSONObject("services");
                            if (serviesObject.has("links")) {
                                Links_ links_ = new Links_();
                                JSONObject linksObject = serviesObject.getJSONObject("links");
                                if (linksObject.has("related")) {
                                    links_.setRelated(linksObject.optString("related"));
                                }
                                services.setLinks(links_);
                            }
                            relationships.setServices(services);
                        }

                        data.setRelationships(relationships);
                    }

                    miviObjet.setData(data);


                }


                if (obj.has("included")) {
                    ArrayList<Included> mIncludedArrayList = new ArrayList<>();

                    Relationships_ relationships_ = new Relationships_();
                    JSONArray includedJsonArray = obj.optJSONArray("included");
                    for (int i = 0; i < includedJsonArray.length(); i++) {
                        JSONObject includeJsonObject = includedJsonArray.getJSONObject(i);
                        Included included = new Included();

                        if (includeJsonObject.has("type")) {
                            included.setType(includeJsonObject.optString("type"));

                        }
                        if (includeJsonObject.has("id"))
                            included.setId(includeJsonObject.optString("id"));
                        if (includeJsonObject.has("attributes")) {
                            JSONObject attributesObject = includeJsonObject.optJSONObject("attributes");


                            Attributes_ attributes_ = new Attributes_();
                            if (attributesObject.has("msn"))
                                attributes_.setMsn(attributesObject.optString("msn"));
                            if (attributesObject.has("credit"))
                                attributes_.setCredit(Integer.parseInt(attributesObject.optString("credit")));
                            if (attributesObject.has("credit-expiry"))
                                attributes_.setCreditExpiry(attributesObject.optString("credit-expiry"));
                            if (attributesObject.has("data-usage-threshold"))
                                attributes_.setDataUsageThreshold(attributesObject.optBoolean("data-usage-threshold"));

                            if (attributesObject.has("included-international-talk"))
                                attributes_.setIncludedInternationalTalk(attributesObject.optString("included-international-talk"));

                            if (attributesObject.has("included-data-balance"))
                                attributes_.setIncludedDataBalance(Integer.parseInt(attributesObject.optString("included-data-balance")));
                            if (attributesObject.has("included-credit-balance"))
                                attributes_.setIncludedCreditBalance(attributesObject.optString("included-credit-balance"));
                            if (attributesObject.has("included-rollover-credit-balance"))
                                attributes_.setIncludedRolloverCreditBalance(attributesObject.optString("included-rollover-credit-balance"));
                            if (attributesObject.has("included-rollover-data-balance"))
                                attributes_.setIncludedRolloverDataBalance(attributesObject.optString("included-rollover-data-balance"));
                            if (attributesObject.has("included-international-talk-balance"))
                                attributes_.setIncludedInternationalTalkBalance(attributesObject.optString("included-international-talk-balance"));
                            if (attributesObject.has("expiry-date"))
                                attributes_.setExpiryDate(attributesObject.optString("expiry-date"));
                            if (attributesObject.has("auto-renewal"))
                                attributes_.setAutoRenewal(attributesObject.optBoolean("auto-renewal"));
                            if (attributesObject.has("primary-subscription"))
                                attributes_.setPrimarySubscription(attributesObject.optBoolean("primary-subscription"));


                            if (attributesObject.has("name"))
                                attributes_.setName(attributesObject.optString("name"));
                            if (attributesObject.has("included-data"))
                                attributes_.setIncludedData(attributesObject.optString("included-data"));
                            if (attributesObject.has("included-credit"))
                                attributes_.setIncludedCredit(attributesObject.optString("included-credit"));
                            if (attributesObject.has("unlimited-text"))
                                attributes_.setUnlimitedText(attributesObject.optBoolean("unlimited-text"));
                            if (attributesObject.has("unlimited-talk"))
                                attributes_.setUnlimitedTalk(attributesObject.optBoolean("unlimited-talk"));
                            if (attributesObject.has("unlimited-international-text"))
                                attributes_.setUnlimitedInternationalText(attributesObject.optBoolean("unlimited-international-text"));
                            if (attributesObject.has("unlimited-international-talk"))
                                attributes_.setUnlimitedInternationalTalk(attributesObject.optBoolean("unlimited-international-talk"));
                            if (attributesObject.has("price"))
                                attributes_.setPrice(Integer.parseInt(attributesObject.optString("price")));

                            included.setAttributes(attributes_);

                        }
                        if (includeJsonObject.has("links")) {
                            JSONObject linksObject = includeJsonObject.optJSONObject("links");
                            if (linksObject.has("self")) {
                                Links__ links__ = new Links__();
                                links__.setSelf(linksObject.optString("self"));
                                included.setLinks(links__);
                            }
                        }
                        if (includeJsonObject.has("relationships")) {
                            JSONObject relationshipsObject = includeJsonObject.optJSONObject("relationships");

                            if (relationshipsObject.has("service")) {
                                Service service = new Service();
                                JSONObject servieObject = relationshipsObject.optJSONObject("service");
                                Links____ links____ = new Links____();
                                if (servieObject.has("links"))
                                    links____.setRelated(servieObject.optJSONObject("links").optString("related"));
                                service.setLinks(links____);
                                relationships_.setService(service);
                            }


                            if (relationshipsObject.has("product")) {
                                Product product = new Product();

                                JSONObject productObject = relationshipsObject.optJSONObject("product");
                                if (productObject.has("data")) {
                                    JSONObject dataObject = productObject.optJSONObject("data");
                                    Data_ data_ = new Data_();
                                    if (dataObject.has("type"))
                                        data_.setType(dataObject.optString("type"));
                                    if (dataObject.has("id"))
                                        data_.setId(dataObject.optString("id"));
                                    product.setData(data_);
                                }
                                relationships_.setProduct(product);
                            }


                            if (relationshipsObject.has("downgrade")) {
                                Downgrade downgrade = new Downgrade();

                                JSONObject downgradeObject = relationshipsObject.optJSONObject("downgrade");
                                if (downgradeObject.has("data"))
                                    downgrade.setData(downgradeObject.optString("data"));
                                relationships_.setDowngrade(downgrade);
                            }

                            if (relationshipsObject.has("subscriptions")) {
                                Subscriptions subscriptions = new Subscriptions();

                                JSONObject subscriptionsObject = relationshipsObject.optJSONObject("subscriptions");
                                if (subscriptionsObject.has("links")) {
                                    Links___ links___ = new Links___();
                                    links___.setRelated(subscriptionsObject.optJSONObject("links").optString("related"));
                                    subscriptions.setLinks(links___);
                                }
                                if (subscriptionsObject.has("data")) {
                                    ArrayList<Datum> mDataArrayList = new ArrayList<>();
                                    Datum subscriptionData = new Datum();
                                    JSONArray dataArray = subscriptionsObject.getJSONArray("data");
                                    for (int j = 0; j < dataArray.length(); j++) {
                                        JSONObject dataObj = dataArray.getJSONObject(i);
                                        if (dataObj.has("type"))
                                            subscriptionData.setType(dataObj.optString("type"));
                                        if (dataObj.has("id"))
                                            subscriptionData.setId(dataObj.optString("id"));

                                        mDataArrayList.add(subscriptionData);
                                    }
                                    subscriptions.setData(mDataArrayList);
                                }

                                relationships_.setSubscriptions(subscriptions);
                            }

                            included.setRelationships(relationships_);

                        }


                        mIncludedArrayList.add(included);
                    }

                    miviObjet.setIncluded(mIncludedArrayList);
                }

                return miviObjet;
            }

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return miviObjet;

    }

    public Observable<MiviObjet> getObservedData() {
        return Observable.defer(new Func0<Observable<MiviObjet>>() {
            @Override
            public Observable<MiviObjet> call() {
                try {
                    return Observable.just(loadJSONFromAsset(getAssets().open("collections.json")));
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        });

    }

    private void makeToast(String message) {
        //Toast.makeText(this, "We are in : " + message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}
