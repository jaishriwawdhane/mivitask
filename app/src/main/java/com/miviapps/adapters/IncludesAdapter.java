package com.miviapps.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.miviapps.R;
import com.miviapps.mivi.Included;

import java.util.List;

/**
 * Created by jaishri on 14/10/18.
 */

public class IncludesAdapter extends RecyclerView.Adapter<IncludesAdapter.IncludesViewHolder> {
    private List<Included> includedList;
    private Context mContext;


    public IncludesAdapter(Context mContext, List<Included> includedList) {
        this.includedList = includedList;
        this.mContext = mContext;

    }

    @Override
    public IncludesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.list_row, parent, false);
        return new IncludesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(IncludesViewHolder holder, int position) {
        Included included = includedList.get(position);
        holder.itemNo.setText((position+1) + ":"+includedList.get(position).getType());
        holder.idValue.setText(includedList.get(position).getId());
        holder.typeValue.setText(includedList.get(position).getType());
        /*if(includedList.get(position).getLinks().getSelf()!=null) {
            Log.d("Link", includedList.get(position).getLinks().getSelf());
            holder.linkValue.setText(includedList.get(position).getLinks().getSelf());
        }else
        {
            holder.linkValue.setText("NA");
        }*/

        if (includedList.get(position).getRelationships() != null) {
            if (includedList.get(position).getRelationships().getService() != null)
                holder.serviceValue.setText("Services");
            else{
                /*holder.serviceValue.setVisibility(View.GONE);
                holder.serviceRelation.setVisibility(View.GONE);*/
                holder.sl1.setVisibility(View.GONE);
            }

            if (includedList.get(position).getRelationships().getDowngrade() != null)
                holder.downgradeValue.setText("downgrade");
            else {
                holder.sl3.setVisibility(View.GONE);
                /*holder.downgradeValue.setVisibility(View.GONE);
                holder.downgradeRelation.setVisibility(View.GONE);*/
            }

            if (includedList.get(position).getRelationships().getProduct() != null)
                holder.productValue.setText("Product");
            else{
                /*holder.productValue.setVisibility(View.GONE);
                holder.productRelation.setVisibility(View.GONE);*/
                holder.sl2.setVisibility(View.GONE);
            }

            if (includedList.get(position).getRelationships().getSubscriptions() != null)
                holder.subscribeValue.setText("Subscription");
            else {
                holder.sl4.setVisibility(View.GONE);
                /*holder.subscribeValue.setVisibility(View.GONE);
                holder.subscribeRelation.setVisibility(View.GONE);
         */   }


        }

        if (includedList.get(position).getAttributes().getCreditExpiry() != null)
            holder.creditExpiryValue.setText(included.getAttributes().getCreditExpiry());
        else
            holder.creditExpiryValue.setVisibility(View.GONE);

        if (includedList.get(position).getAttributes().getMsn() != null)
            holder.msnValue.setText(included.getAttributes().getMsn());
        else
            holder.msnValue.setVisibility(View.GONE);

        if (includedList.get(position).getAttributes().isDataUsageThreshold())
            holder.dataUsageThresholdValue.setText(String.valueOf(included.getAttributes().isDataUsageThreshold()));
        else
            holder.dataUsageThresholdValue.setVisibility(View.GONE);
        if (includedList.get(position).getAttributes().getName()!=null)
            holder.name.setText(String.valueOf(included.getAttributes().getName()));
        else
            holder.name.setVisibility(View.GONE);
        if (String.valueOf(includedList.get(position).getAttributes().getPrice())!=null)
            holder.price.setText(String.valueOf(included.getAttributes().getPrice()));
        else
            holder.price.setVisibility(View.GONE);
    }



    @Override
    public int getItemCount() {
        return includedList.size();
    }

    public class IncludesViewHolder extends RecyclerView.ViewHolder {
        TextView itemNo;
        TextView typeValue;
        TextView idValue;
        TextView linkValue;
        TextView serviceValue;
        TextView productValue;
        TextView downgradeValue;
        TextView subscribeValue;
        TextView msnValue;
        TextView creditExpiryValue;
        TextView dataUsageThresholdValue;
        TextView includedDataBalanceValue;
        TextView serviceRelation;
        TextView productRelation;
        TextView downgradeRelation;
        TextView subscribeRelation;
        TextView name, price, primarySubScription;

        LinearLayout sl1, sl2, sl3, sl4;

        public IncludesViewHolder(View itemView) {
            super(itemView);
            sl1 = itemView.findViewById(R.id.sl1);
            sl2 = itemView.findViewById(R.id.sl2);
            sl3 = itemView.findViewById(R.id.sl3);
            sl4 = itemView.findViewById(R.id.sl4);
            itemNo = itemView.findViewById(R.id.itemNo);
            typeValue = itemView.findViewById(R.id.typeValue);
            idValue = itemView.findViewById(R.id.idValue);
            includedDataBalanceValue = itemView.findViewById(R.id.includedDataBalanceValue);
            dataUsageThresholdValue = itemView.findViewById(R .id.dataUsageThresholdValue);
            creditExpiryValue = itemView.findViewById(R.id.creditExpiryValue);
            msnValue = itemView.findViewById(R.id.msnValue);
            subscribeValue = itemView.findViewById(R.id.subscribeValue);
            downgradeValue = itemView.findViewById(R.id.downgradeValue);
            productValue = itemView.findViewById(R.id.productValue);

            serviceValue = itemView.findViewById(R.id.serviceValue);
            linkValue = itemView.findViewById(R.id.linkValue);
            name = itemView.findViewById(R.id.nameValue);
            price = itemView.findViewById(R.id.priceValue);
            primarySubScription = itemView.findViewById(R.id.primarySubscriptionValue);

            serviceRelation = itemView.findViewById(R.id.serviceRelation);
            productRelation = itemView.findViewById(R.id.productRelation);
            downgradeRelation = itemView.findViewById(R.id.downgradeRelation);
            subscribeRelation = itemView.findViewById(R.id.subscribeRelation);

        }
    }
}
